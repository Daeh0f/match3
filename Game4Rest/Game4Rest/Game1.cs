using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game4Rest
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        const int WIN_HEIGHT = 600;
        const int WIN_WIDTH = 1000;

        GraphicsDeviceManager graphics;
        static public SpriteBatch spriteBatch;

        enum State { MainMenu, Game, GameOver };

        SpriteFont Font;
        State CurrentState = State.MainMenu;
        Texture2D BackgroundTexture;
        Texture2D GameoverWindowTexture;
        List<Texture2D> GemTexture = new List<Texture2D>();
        static public List<Texture2D> SparksTexture = new List<Texture2D>();
        static public List<ParticleEngine> SparkGenerator = new List<ParticleEngine>();

        static public MouseState oldMouseState = new MouseState();
        static public MouseState currentMouseState = new MouseState();
        GameBoard gb;
        SpriteButton playBt, okBt;
        UInt64 gameStartTime = 0;
        bool newGameFlag = true;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = WIN_HEIGHT;
            graphics.PreferredBackBufferWidth = WIN_WIDTH;

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            this.IsMouseVisible = true;


            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            BackgroundTexture = Content.Load<Texture2D>("green-background");
            GameoverWindowTexture = Content.Load<Texture2D>("gameover-window");

            Gem.GemTextureList[0] = Content.Load<Texture2D>("gems/ruby");
            Gem.GemTextureList[1] = Content.Load<Texture2D>("gems/ruby2");
            Gem.GemTextureList[2] = Content.Load<Texture2D>("gems/sapphire");
            Gem.GemTextureList[3] = Content.Load<Texture2D>("gems/topaz");
            Gem.GemTextureList[4] = Content.Load<Texture2D>("gems/swarovski-crystal");
            Gem.GemTextureList[5] = Content.Load<Texture2D>("gems/diamond");

            SparksTexture.Add(Content.Load<Texture2D>("sparks/diamond"));
            SparksTexture.Add(Content.Load<Texture2D>("sparks/star"));
            SparksTexture.Add(Content.Load<Texture2D>("sparks/circle"));

            Font = Content.Load<SpriteFont>("SimpleFont");

            Texture2D texture = Content.Load<Texture2D>("tile");
            gb = new GameBoard(texture, 8,
                       new Vector2(WIN_WIDTH / 2 - texture.Width * 4,
                                   WIN_HEIGHT / 2 - texture.Height * 4));

            texture = Content.Load<Texture2D>("play-button");
            playBt = new SpriteButton(texture,
                                      new Rectangle(WIN_WIDTH / 2 - (int)(WIN_WIDTH * 0.15),
                                                    WIN_HEIGHT / 2 - (int)(WIN_HEIGHT * 0.1),
                                                    (int)(WIN_WIDTH * 0.3),
                                                    (int)(WIN_HEIGHT * 0.3)),
                                      Color.Aqua,  
                                      Color.White);

            texture = Content.Load<Texture2D>("ok-button");
            okBt = new SpriteButton(texture,
                                    new Rectangle(WIN_WIDTH / 2 - (int)(WIN_WIDTH * 0.1),
                                                  WIN_HEIGHT / 2 - (int)(WIN_HEIGHT * 0.05),
                                                  (int)(WIN_WIDTH * 0.2),
                                                  (int)(WIN_HEIGHT * 0.1)),
                                    Color.White,
                                    new Color(230, 230, 230));
        }

        protected override void UnloadContent()
        {
            
        }


        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();


            currentMouseState = Mouse.GetState();
            switch (CurrentState)
            {
                case State.MainMenu:
                    /*
                     * Button update method return true if cursor on button
                    */
                    if ( playBt.Update() )
                    {
                        if (currentMouseState.LeftButton == ButtonState.Pressed && oldMouseState.LeftButton == ButtonState.Released)
                        {
                            CurrentState = State.Game;
                            gameStartTime = (UInt64)(gameTime.TotalGameTime.Minutes * 60 + gameTime.TotalGameTime.Seconds);
                            newGameFlag = true;
                        }
                    }
                    break;

                case State.Game:
                    if (newGameFlag)
                    {
                        Texture2D texture = Content.Load<Texture2D>("tile");
                        gb = new GameBoard(texture, 8,
                                   new Vector2(WIN_WIDTH / 2 - texture.Width * 4,
                                               WIN_HEIGHT / 2 - texture.Height * 4));
                        newGameFlag = false;
                    }

                    gb.CurrentGameTime =  (UInt64) (gameTime.TotalGameTime.Minutes*60 + gameTime.TotalGameTime.Seconds) - gameStartTime;
                    gb.Update(currentMouseState);


                    if (gb.CurrentGameTime >= gb.TimeToGameover)
                    {
                        CurrentState = State.GameOver;
                    }
                    break;

                case State.GameOver:
                    if ( okBt.Update() )
                    {
                        if (currentMouseState.LeftButton == ButtonState.Pressed && oldMouseState.LeftButton == ButtonState.Released)
                        {
                            CurrentState = State.MainMenu;
                        }
                    }
                    break;
            }

            foreach (ParticleEngine eng in SparkGenerator)
                eng.Update();


            oldMouseState = Mouse.GetState();

            base.Update(gameTime);
        }


        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);


            spriteBatch.Begin();

            spriteBatch.Draw(BackgroundTexture, 
                             new Rectangle(0, 0, WIN_WIDTH, WIN_HEIGHT), 
                             Color.White);

            switch (CurrentState)
            {
                case State.MainMenu:
                    playBt.Draw();
                    break;

                case State.Game:
                    spriteBatch.DrawString(Font, "Time:", new Vector2(10, 10), Color.Black);
                    spriteBatch.DrawString(Font, (gb.TimeToGameover - gb.CurrentGameTime).ToString(), new Vector2(10, 30), Color.Black);
                    spriteBatch.DrawString(Font, "Score:", new Vector2(WIN_WIDTH - 100, 10), Color.Black);
                    spriteBatch.DrawString(Font, gb.CurrentScore.ToString(), new Vector2(WIN_WIDTH - 100, 30), Color.Black);
                    gb.Draw();
                    break;

                case State.GameOver:
                    spriteBatch.Draw(GameoverWindowTexture, 
                                     new Rectangle(WIN_WIDTH / 2 - GameoverWindowTexture.Width / 2,
                                                   WIN_HEIGHT / 2 - GameoverWindowTexture.Height / 2 - 50,
                                                   GameoverWindowTexture.Width,
                                                   GameoverWindowTexture.Height), 
                                     Color.White);
                    okBt.Draw();
                    break;
            }


            spriteBatch.End();

            for (int i = 0; i < SparkGenerator.Count; i++)
            {
                SparkGenerator[i].Draw(spriteBatch);
            }

            base.Draw(gameTime);
        }
    }
}
