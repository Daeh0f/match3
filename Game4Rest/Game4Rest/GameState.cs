﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace Game4Rest
{
    static class GameState
    {
        public enum State { MainMenu, Game, GameOver };
        static public State CurrentState = State.MainMenu;
        static public Texture2D BackgroundTexture;
        static public Texture2D PlayButtonTexture;
        static public Texture2D TileTexture;
        static public List<Texture2D> GemTexture = new List<Texture2D>();

        static private Rectangle playButtonRectangle = new Rectangle(100, 100, 200, 150);
        static private Color playButtonColor = Color.White;
        static private MouseState oldMouseState = new MouseState();



        static public void Update()
        {
            MouseState currentMouseState = Mouse.GetState();
            
            switch (CurrentState)
            {
                case State.MainMenu:
                    if (playButtonRectangle.Intersects(new Rectangle(currentMouseState.X,
                                                                     currentMouseState.Y,
                                                                     currentMouseState.X,
                                                                     currentMouseState.Y)))
                    {
                        playButtonColor = Color.Yellow;

                        if (currentMouseState.LeftButton == ButtonState.Pressed && oldMouseState.LeftButton == ButtonState.Released)
                        {
                            GameState.CurrentState = State.Game;
                        }
                    }
                    else 
                    {
                        playButtonColor = Color.White;
                    }
                    break;

                case State.Game:
                    break;

                case State.GameOver:

                    break;
            }

            oldMouseState = Mouse.GetState();
        }

        static public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();

            spriteBatch.Draw(BackgroundTexture, new Rectangle(0, 0, 800, 480), Color.White);

            switch (CurrentState)
            {
                case State.MainMenu:
                    spriteBatch.Draw(PlayButtonTexture, playButtonRectangle, playButtonColor);
                break;

                case State.Game:
                break;

                case State.GameOver:

                break;
            }

            spriteBatch.End();
        }
    }
}
