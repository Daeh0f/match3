using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Game4Rest
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        const int WIN_HEIGHT = 600;
        const int WIN_WIDTH = 800;

        GraphicsDeviceManager graphics;
        static public SpriteBatch spriteBatch;

        enum State { MainMenu, Game, GameOver };

        State CurrentState = State.MainMenu;
        Texture2D BackgroundTexture;
        Texture2D GameoverWindowTexture;
        List<Texture2D> GemTexture = new List<Texture2D>();

        Rectangle playButtonRectangle;
        Rectangle okButtonRectangle;
        Color okButtonColor = Color.Green;
        Color playButtonColor = Color.Aqua;
        MouseState oldMouseState = new MouseState();

        GameBoard gb;

        SpriteButton playBt, okBt;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = WIN_HEIGHT;
            graphics.PreferredBackBufferWidth = WIN_WIDTH;

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            this.IsMouseVisible = true;


            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            BackgroundTexture = Content.Load<Texture2D>("green-background");
            TileTexture = Content.Load<Texture2D>("tile");
            GameoverWindowTexture = Content.Load<Texture2D>("gameover");

            GemTexture.Add(Content.Load<Texture2D>("gems/ruby"));
            GemTexture.Add(Content.Load<Texture2D>("gems/ruby2"));
            GemTexture.Add(Content.Load<Texture2D>("gems/sapphire"));
            GemTexture.Add(Content.Load<Texture2D>("gems/topaz"));
            GemTexture.Add(Content.Load<Texture2D>("gems/swarovski-crystal"));
            GemTexture.Add(Content.Load<Texture2D>("gems/diamond"));

            Gem.GemTextureList = GemTexture;

             okButtonRectangle = new Rectangle(WIN_WIDTH / 2 - 100,
                                              WIN_HEIGHT / 2 - 30,
                                              OkButtonTexture.Width,
                                              OkButtonTexture.Height);
            

            playButtonRectangle = new Rectangle(WIN_WIDTH  / 2 - 100,
                                                WIN_HEIGHT / 2 - 75,
                                                200,
                                                150);

            //gb = new GameBoard(TileTexture, 6, new Vector2(200, 10), spriteBatch);

            Texture2D texture = Content.Load<Texture2D>("play-button");
            playBt = new SpriteButton(texture,
                                      new Rectangle(WIN_WIDTH / 2 - 100, WIN_HEIGHT / 2 - 30, texture.Width, texture.Height), 
                                      Color.Green, 
                                      Color.White);

            texture = Content.Load<Texture2D>("ok-button");
            okBt = new SpriteButton(texture, 
                                    new Rectangle(WIN_WIDTH  / 2 - 100, WIN_HEIGHT / 2 - 75, 200, 150),
                                    Color.White, 
                                    Color.Gray);
        }

        protected override void UnloadContent()
        {
            
        }


        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            MouseState currentMouseState = Mouse.GetState();


            switch (CurrentState)
            {
                case State.MainMenu:
                    if (playButtonRectangle.Intersects(new Rectangle(currentMouseState.X,
                                                                     currentMouseState.Y,
                                                                     1,
                                                                     1)))
                    {
                        playButtonColor = Color.White;

                        if (currentMouseState.LeftButton == ButtonState.Pressed && oldMouseState.LeftButton == ButtonState.Released)
                        {
                            CurrentState = State.Game;
                        }
                    }
                    else
                    {
                        playButtonColor = Color.Aqua;
                    }
                    break;

                case State.Game:
                    break;

                case State.GameOver:
                    if ( okButtonRectangle.Intersects(new Rectangle(currentMouseState.X, currentMouseState.Y, 1, 1)) )
                    {
                        okButtonColor = Color.White;

                        if (currentMouseState.LeftButton == ButtonState.Pressed && oldMouseState.LeftButton == ButtonState.Released)
                        {
                            CurrentState = State.MainMenu;
                        }
                    }
                    else
                    {
                        okButtonColor = Color.Gray;
                    }
                    break;
            }

            oldMouseState = Mouse.GetState();

            base.Update(gameTime);
        }


        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            

            spriteBatch.Begin();

            spriteBatch.Draw(BackgroundTexture, 
                             new Rectangle(0, 0, GameWindowWidth, GameWindowHeight), 
                             Color.White);

            switch (CurrentState)
            {
                case State.MainMenu:
                    spriteBatch.Draw(PlayButtonTexture, playButtonRectangle, playButtonColor);
                    break;

                case State.Game:
                    gb.Draw();
                    break;

                case State.GameOver:
                    spriteBatch.Draw(GameoverWindowTexture, 
                                     new Rectangle(140, 60, GameoverWindowTexture.Width, GameoverWindowTexture.Height), 
                                     Color.White);
                    spriteBatch.Draw(OkButtonTexture, okButtonRectangle, okButtonColor);
                    break;
            }


            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
