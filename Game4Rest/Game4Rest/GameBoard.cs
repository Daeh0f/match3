﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Game4Rest
{
    class GameBoard
    {
        Gem[,] gems;
        public
        SpriteButton[,] cells;

        Vector2 position;
        Vector3 selectedGem;
        int size;
        Random r;

        public UInt64 CurrentScore;
        public UInt64 TimeToGameover;
        public UInt64 CurrentGameTime;

        public GameBoard(Texture2D cellTexture, int size, Vector2 position, UInt64 ttg = 60)
        {
            r = new Random();
            selectedGem = new Vector3(-1, -1, 0);

            gems  = new Gem[size, size];
            cells = new SpriteButton[size, size];

            this.size = size;
            this.position = position;
            TimeToGameover = ttg;
            CurrentScore = 0;

            Vector2 v = position;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    gems[i, j]  = new Gem((Gem.GemType) r.Next(6));
                    cells[i, j] = new SpriteButton(cellTexture, 
                                                   new Rectangle((int)v.X, (int)v.Y, cellTexture.Width, cellTexture.Height), 
                                                   Color.White,
                                                   new Color(200, 200, 200));
                    v.X += cellTexture.Width + 1;
                }
                v.X = position.X;
                v.Y += cellTexture.Height + 1;
            }
        }

        public void Draw()
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    //Game1.spriteBatch.Draw(cells[i, j].Texture, cells[i, j].Area, new Color(200, 200, 200));
                    cells[i, j].Draw();
                    /*
                    Game1.spriteBatch.Draw(gems[i, j].GemTexture,
                                           new Vector2(cells[i, j].Area.X + (cells[i, j].Area.Width - gems[i, j].GemTexture.Width) / 2,
                                                       cells[i, j].Area.Y + (cells[i, j].Area.Height - gems[i, j].GemTexture.Height) / 2),
                                           Color.White );*/
                    //Game1.spriteBatch.Draw(_board[i, j].GemTexture, new Vector2(currentPos.X + 13, currentPos.Y + 13), Color.White);
                    
                }
            }

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    float angle = 0;
                    if (selectedGem.X == i & selectedGem.Y == j) angle = selectedGem.Z;

                    if (gems[i, j].Type != Gem.GemType.Empty)
                    {
                        Game1.spriteBatch.Draw(gems[i, j].GemTexture,
                                               new Vector2(cells[i, j].Area.X + cells[i, j].Area.Width / 2,
                                                           cells[i, j].Area.Y + cells[i, j].Area.Height / 2),
                                               new Rectangle(0, 0, gems[i, j].GemTexture.Width, gems[i, j].GemTexture.Height),
                                               Color.White,
                                               angle,
                                               new Vector2(gems[i, j].GemTexture.Width / 2, gems[i, j].GemTexture.Height / 2),
                                               1.0f,
                                               SpriteEffects.None,
                                               1);
                    }
                }
            }

        }

        public void Update(MouseState ms)
        {
            selectedGem.Z += 0.1f;
            //checkMatch();
            checkLongMatch();

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    // клетка под курсором
                    if (cells[i, j].Update())
                    {
                        if (Game1.currentMouseState.LeftButton == ButtonState.Pressed 
                        &&  Game1.oldMouseState.LeftButton     == ButtonState.Released)
                        {
                            if (selectedGem.X == i & selectedGem.Y == j)
                            {
                                selectedGem = new Vector3(-1, -1, 0);
                                continue;
                            }

                            if (selectedGem.X == -1 & selectedGem.Y == -1)
                            {
                                selectedGem = new Vector3(i, j, 0);
                                continue;
                            }

                            if (isNear((int)selectedGem.X, (int)selectedGem.Y, i, j))
                            {
                                Gem temp = gems[i, j];
                                gems[i, j] = gems[(int)selectedGem.X, (int)selectedGem.Y];
                                gems[(int)selectedGem.X, (int)selectedGem.Y] = temp;

                                if (!checkLongMatch())
                                {
                                    temp = gems[i, j];
                                    gems[i, j] = gems[(int)selectedGem.X, (int)selectedGem.Y];
                                    gems[(int)selectedGem.X, (int)selectedGem.Y] = temp;
                                }
                            }
                            selectedGem = new Vector3(-1, -1, 0);
                        }

                    }
                }
            }


            compeleteColumn();
        }

        private bool checkMatch()
        {
            bool anyMatches = false;

            for (int i = size - 2; i >= 1; i--)
            {
                for (int j = 0; j < size; j++)
                {
                    if (gems[i, j].Type == gems[i - 1, j].Type
                    && gems[i, j].Type == gems[i + 1, j].Type)
                    {
                        //CurrentScore += gems[i, j].Price * 3;
                        //gems[i, j] = new Gem(Gem.GemType.Empty);
                        destroyGem(i, j);
                        //gems[i+1, j] = new Gem(Gem.GemType.Empty);
                        destroyGem(i + 1, j);
                        //gems[i-1, j] = new Gem(Gem.GemType.Empty);
                        destroyGem(i - 1, j);

                        anyMatches = true;
                    }
                }
            }


            for (int i = size - 1; i >= 0; i--)
            {
                for (int j = 1; j < size - 1; j++)
                {
                    if (gems[i, j].Type == gems[i, j - 1].Type
                    && gems[i, j].Type == gems[i, j + 1].Type)
                    {
                        //CurrentScore += gems[i, j].Price * 3;
                        //gems[i, j] = new Gem(Gem.GemType.Empty);
                        destroyGem(i, j);
                        //gems[i, j - 1] = new Gem(Gem.GemType.Empty);
                        destroyGem(i, j - 1);
                        //gems[i, j + 1] = new Gem(Gem.GemType.Empty);
                        destroyGem(i, j + 1);

                        anyMatches = true;
                    }
                }
            }

            return anyMatches;
        }


        private bool isNear(int x1, int y1, int x2, int y2)
        {
            return ( ( x1 + 1 == x2 & y1 == y2)  |
                     ( x1 - 1 == x2 & y1 == y2)  |
                     ( y1 + 1 == y2 & x1 == x2)  |
                     ( y1 - 1 == y2 & x1 == x2) );
        }

        private void compeleteColumn()
        {
            for (int column = 0; column < size; column++ )
            {
                for (int row = size - 2; row >= 0; row--)
                {
                    //if (gems[row, column].Type == Gem.GemType.Empty) firstEmtyGemPos = row;
                    if (gems[row + 1, column].Type == Gem.GemType.Empty)
                    {
                        gems[row + 1, column] = gems[row, column];
                        gems[row, column] = new Gem(Gem.GemType.Empty);
                    }
                }

                if (gems[0, column].Type == Gem.GemType.Empty) 
                    gems[0, column] = new Gem((Gem.GemType) r.Next(6));
            }
        }

        private void destroyGem(int i, int j)
        {
            CurrentScore += gems[i, j].Price;
            gems[i, j] = new Gem(Gem.GemType.Empty);
            Game1.SparkGenerator.Add(new ParticleEngine(new Vector2(cells[i, j].Area.X + cells[i, j].Area.Width / 2,
                                                                    cells[i, j].Area.Y + cells[i, j].Area.Height / 2)));
        }

        private bool checkLongMatch()
        {
            List< List<Vector2> > matchList = new List< List<Vector2> >();

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    List<Vector2> match = getMatchH(i, j);      
                    if (match.Count > 2) 
                    {       
                        matchList.Add(match);       
                        j += match.Count-1;      
                    }
                }
            }

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    List<Vector2> match = getMatchV(i, j);
                    if (match.Count > 2)
                    {
                        matchList.Add(match);
                        i += match.Count - 1;
                    }
                }
            }


            foreach ( List<Vector2> line in matchList )
            {
                foreach (Vector2 element in line)
                {
                    destroyGem((int)element.X, (int)element.Y);
                }
            }

            return (matchList.Count > 0);
        }

        private List<Vector2> getMatchV(int row, int col)
        {
            List<Vector2> matchList = new List<Vector2>();

            matchList.Add(new Vector2(row, col));
            for (int i = row; i < size - 1; i++)
            {
                if (gems[row, col].Type == gems[i + 1, col].Type)
                {
                    matchList.Add(new Vector2(1 + i, col));
                }
                else
                {
                    return matchList;
                } 
            }

            return matchList;
        }

        private List<Vector2> getMatchH(int row, int col)
        {
            List<Vector2> matchList = new List<Vector2>();

            matchList.Add(new Vector2(row, col));
            for (int i = col; i < size - 1; i++)
            {
                if (gems[row, col].Type == gems[row, i + 1].Type)
                {
                    matchList.Add(new Vector2(row, i + 1));
                }
                else
                {
                    return matchList;
                }
            }

            return matchList;
        }
    }
}
