﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Game4Rest
{
    class Gem
    {
        static public Texture2D[] GemTextureList = new Texture2D[6];

        public enum GemType { Ruby, Ryby2, Sapphire, Topaz, Diamond, Swarovski, Empty };

        public Texture2D GemTexture;
        public GemType Type;
        public UInt64 Price;

        public Gem(GemType type)
        {
            Type = type;
            switch (type)
            {
                case GemType.Ruby:
                    GemTexture = GemTextureList[0];
                    Price = 200;
                    break;
                case GemType.Ryby2:
                    GemTexture = GemTextureList[1];
                    Price = 100;
                    break;
                case GemType.Sapphire:
                    GemTexture = GemTextureList[2];
                    Price = 100;
                    break;
                case GemType.Topaz:
                    GemTexture = GemTextureList[3];
                    Price = 100;
                    break;
                case GemType.Swarovski:
                    GemTexture = GemTextureList[4];
                    Price = 200;
                    break;
                case GemType.Diamond:
                    GemTexture = GemTextureList[5];
                    Price = 300;
                    break;
                case GemType.Empty:
                    Price = 0;
                    break;
            }
        }
    }
}
