﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Game4Rest
{
    class SpriteButton
    {
        public Texture2D Texture;
        Color ActiveColor, PassiveColor;
        public Rectangle Area;
        bool bActive = false;

        public SpriteButton(Texture2D texture, Rectangle area, Color active, Color passive)
        {
            Texture = texture;
            Area = area;
            ActiveColor = active;
            PassiveColor = passive;
        }

        public bool Update()
        {
            return bActive = Active(Game1.currentMouseState);
        }

        public void Draw()
        {
            if (bActive)
            {
                Game1.spriteBatch.Draw(Texture, Area, ActiveColor);
            }
            else
            {
                Game1.spriteBatch.Draw(Texture, Area, PassiveColor);
            }
        }

        bool Active(MouseState curMouseState)
        {
            if (Area.Intersects(new Rectangle(curMouseState.X, curMouseState.Y, 1, 1)))
            {
                return true;
            }
            return false;
        }
    }
}
